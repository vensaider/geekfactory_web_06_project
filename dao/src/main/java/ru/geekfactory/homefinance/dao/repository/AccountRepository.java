package ru.geekfactory.homefinance.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.geekfactory.homefinance.dao.model.AccountModel;

import java.util.List;

public interface AccountRepository extends JpaRepository<AccountModel, Long> {
    AccountModel findAccountModelByName(String name);
}
