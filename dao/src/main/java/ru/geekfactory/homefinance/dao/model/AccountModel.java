package ru.geekfactory.homefinance.dao.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


@Entity
@Table(name = "account_tbl")
@Getter
@Setter
@Data
public class AccountModel {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long accountId;

    @Column(name = "name")
    private String name;

    @Column(name = "account_type")
    @Enumerated(EnumType.STRING)
    private AccountType type;

    @Column(name = "amount")
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currencyId")
    private CurrencyModel currencyModel;

    @OneToMany(mappedBy = "accountModel",fetch = FetchType.EAGER)
    @JsonIgnore
    private List<TransactionModel> transaction;

    @OneToOne(mappedBy = "accountModel",fetch = FetchType.EAGER)
    private CardModel cardModel;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private UserModel userModel;
}
