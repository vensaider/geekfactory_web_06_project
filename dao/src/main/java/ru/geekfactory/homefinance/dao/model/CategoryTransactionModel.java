package ru.geekfactory.homefinance.dao.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "category_tbl")
@Getter
@Setter
@Data
public class CategoryTransactionModel {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long categoryId;

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private TypeCategory typeCategory;
}
