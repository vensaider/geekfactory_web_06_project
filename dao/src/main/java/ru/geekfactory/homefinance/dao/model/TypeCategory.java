package ru.geekfactory.homefinance.dao.model;

public enum TypeCategory {
    COMMON("COMMON"),SERVICE("SERVICE"),SALARY("SALARY");

    private String name;

    TypeCategory(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
