package ru.geekfactory.homefinance.dao.model;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "credit_card_tbl")
@Getter
@Setter
@Data
public class CardModel {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long cardId;

    @Column(name = "card_owner")
    private String owner;

    @Column(name = "card_number")
    private int cardNumber;

    @Column(name = "card_valid_date")
    @Type(type = "org.hibernate.type.LocalDateTimeType")
    private LocalDateTime validDate;

    @Column(name = "card_cv_code")
    private String cvCode;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId")
    private AccountModel accountModel;
}
