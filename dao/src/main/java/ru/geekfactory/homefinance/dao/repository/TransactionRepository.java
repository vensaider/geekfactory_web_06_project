package ru.geekfactory.homefinance.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.geekfactory.homefinance.dao.model.TransactionModel;

public interface TransactionRepository extends JpaRepository<TransactionModel, Long> {
}
