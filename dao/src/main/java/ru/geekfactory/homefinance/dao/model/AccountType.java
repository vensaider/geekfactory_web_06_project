package ru.geekfactory.homefinance.dao.model;

public enum AccountType {

    CASH("CASH"),DEPOSIT("DEPOSIT");

    private String accountType;

    AccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}
