package ru.geekfactory.homefinance.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;

public interface CurrencyRepository extends JpaRepository<CurrencyModel, Long> {
}
