package ru.geekfactory.homefinance.dao.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "currency_tbl")
@Getter
@Setter
@Data
public class CurrencyModel {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long currencyId;

    @Column(name = "code")
    @Enumerated(EnumType.STRING)
    private CurrencyType code;

    @Column(name = "symbol")
    private String symbol;
}
