package ru.geekfactory.homefinance.dao.model;

public enum CurrencyType {
    USD("USD"),EUR("EUR"),GBP("GBP"),JPY("JPY"),CNY("CNY"),RUB("RUB");

    private String code;

    CurrencyType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
