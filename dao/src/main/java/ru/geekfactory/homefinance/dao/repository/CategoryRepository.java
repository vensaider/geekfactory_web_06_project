package ru.geekfactory.homefinance.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TypeCategory;

public interface CategoryRepository extends JpaRepository<CategoryTransactionModel, Long> {
    CategoryTransactionModel findCategoryTransactionModelByTypeCategory(TypeCategory typeCategory);
}
