package ru.geekfactory.homefinance.dao.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "transaction_tbl")
@Getter
@Setter
@Data
public class TransactionModel {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long transactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "accountId")
    private AccountModel accountModel;

    @Column(name = "date")
    @Type(type = "org.hibernate.type.LocalDateTimeType")
    private LocalDateTime operationDate;

    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "transaction_category_tbl",
            joinColumns = { @JoinColumn(name = "transactionId") },
            inverseJoinColumns = { @JoinColumn(name = "categoryId") })
    private List<CategoryTransactionModel> categoryTransaction;
}
