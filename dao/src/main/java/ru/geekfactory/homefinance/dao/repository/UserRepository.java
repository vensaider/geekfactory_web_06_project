package ru.geekfactory.homefinance.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.geekfactory.homefinance.dao.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {
    UserModel findByUserName(String userName);
}
