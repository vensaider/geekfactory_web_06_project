package ru.geekfactory.homefinance.dao.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "user_tbl")
@Getter
@Setter
@Data
public class UserModel {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long userId;

    @Column(name = "userName")
    private String userName;

    @Column(name = "userPasswd")
    private String password;

    @Column(name = "account_type")
    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @OneToMany(mappedBy = "userModel",fetch = FetchType.EAGER)
    @JsonIgnore
    private List<AccountModel> accountModels;
}
