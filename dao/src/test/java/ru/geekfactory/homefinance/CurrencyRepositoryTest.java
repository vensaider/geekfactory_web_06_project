package ru.geekfactory.homefinance;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.model.CurrencyType;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.testconfig.TestDatabaseConfig;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestDatabaseConfig.class)
class CurrencyRepositoryTest {

    private CurrencyModel currencyModel = new CurrencyModel();

    @Autowired
    private CurrencyRepository currencyRepository;

    @PersistenceContext
    private EntityManager em;

    @Test
    void addCurrencyTest(){
        currencyModel.setSymbol("RUB");
        currencyModel.setCode(CurrencyType.RUB);
        currencyRepository.saveAndFlush(currencyModel);
        assertEquals(currencyModel.getCode(),currencyRepository.findById(1L).get().getCode());
    }

    @Test
    void getAllCurrencyTest(){
        currencyModel.setSymbol("RUB");
        currencyModel.setCode(CurrencyType.RUB);
        currencyRepository.saveAndFlush(currencyModel);
        assertEquals(currencyModel,currencyRepository.findAll().get(0));
    }

    @Test
    void getCurrencyById(){
        currencyModel.setSymbol("RUB");
        currencyModel.setCode(CurrencyType.RUB);
        currencyRepository.saveAndFlush(currencyModel);
        assertEquals(currencyModel,currencyRepository.findById(1L).get());
    }

    @Test
    void updateCurrencyTest(){
        currencyModel.setSymbol("RUB");
        currencyModel.setCode(CurrencyType.RUB);
        currencyRepository.saveAndFlush(currencyModel);
        CurrencyModel currencyModelCheck = new CurrencyModel();
        currencyModelCheck.setCurrencyId(1L);
        currencyModelCheck.setCode(CurrencyType.EUR);
        currencyModelCheck.setSymbol("EUR");
        currencyModel.setCode(currencyModelCheck.getCode());
        currencyModel.setSymbol(currencyModelCheck.getSymbol());
        currencyRepository.save(currencyModel);
        assertEquals(currencyModelCheck,currencyRepository.findById(1L).get());
    }

    @Test
    void deleteCurrencyTest(){
        currencyModel.setSymbol("RUB");
        currencyModel.setCode(CurrencyType.RUB);
        currencyRepository.saveAndFlush(currencyModel);
        currencyRepository.deleteById(1L);
        assertNotNull(currencyRepository.findAll());
    }
}