
DROP TABLE IF EXISTS transaction_category_tbl;
DROP TABLE IF EXISTS transaction_tbl;
DROP TABLE IF EXISTS transaction_type_tbl;
DROP TABLE IF EXISTS category_tbl;
DROP TABLE IF EXISTS account_tbl;
DROP TABLE IF EXISTS currency_tbl;

CREATE TABLE currency_tbl (
  currencyId INT AUTO_INCREMENT PRIMARY KEY  NOT NULL,
  code ENUM('USD','EUR','GBP','JPY','CNY','RUB') NOT NULL,
  symbol VARCHAR(50) NOT NULL
);

CREATE TABLE account_tbl (
  accountId INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  account_type ENUM('CASH','DEPOSIT') NOT NULL,
  amount DECIMAL(15, 2) NOT NULL,
  currencyId INT NOT NULL,
  userId INT NOT NULL,

  CONSTRAINT currency_fk FOREIGN KEY(currencyId) REFERENCES currency_tbl(currencyId),
  CONSTRAINT currency_fk FOREIGN KEY(userId) REFERENCES user_tbl(userId)
);

CREATE TABLE user_tbl (
  userId INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  userName VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  userRole ENUM('USER','ADMIN') NOT NULL
);

CREATE TABLE credit_card_tbl (
  carsId INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  card_owner VARCHAR(50) NOT NULL,
  cardNumber INT(16) NOT NULL,
  card_valid_date DATETIME NOT NULL,
  card_cv_code VARCHAR(50) NOT NULL,
  accountId INT NOT NULL,

  CONSTRAINT account_card_fk FOREIGN KEY(accountId) REFERENCES account_tbl(accountId)
);

CREATE TABLE category_tbl (
  categoryId INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name ENUM('COMMON','SERVICE','SALARY') NOT NULL
);

CREATE TABLE transaction_tbl (
  transactionId INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  date DATETIME NOT NULL,
  categoryId INT NOT NULL,
  name VARCHAR(50) NOT NULL,
  accountId INT NOT NULL,

  CONSTRAINT category_fk FOREIGN KEY(categoryId) REFERENCES category_tbl(categoryId),
  CONSTRAINT account_fk FOREIGN KEY(accountId) REFERENCES account_tbl(accountId)
);

CREATE TABLE transaction_category_tbl (
  transactionId INT NOT NULL,
  categoryId INT NOT NULL,

  CONSTRAINT transaction_category_category_fk FOREIGN KEY(categoryId) REFERENCES category_tbl(categoryId),
  CONSTRAINT transaction_category_transaction_fk FOREIGN KEY(transactionId) REFERENCES transaction_tbl(transactionId)
);