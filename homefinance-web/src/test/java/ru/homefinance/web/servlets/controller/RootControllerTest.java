package ru.homefinance.web.servlets.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.homefinance.web.servlets.config.AppInitializer;
import ru.homefinance.web.servlets.config.WebConfig;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AppInitializer.class, WebConfig.class})
@WebAppConfiguration
class RootControllerTest {

    @Autowired
    private RootController rootController;

    @Test
    public void contextLoads() throws Exception {
        assertNotNull(rootController);
    }

}