package ru.homefinance.web.servlets.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.dao.model.TypeCategory;
import ru.geekfactory.homefinance.service.AccountService;
import ru.geekfactory.homefinance.service.CategoryTransactionService;
import ru.geekfactory.homefinance.service.TransactionService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @Autowired
    CategoryTransactionService categoryTransactionService;

    @Autowired
    private AccountService accountService;


    @GetMapping("/transaction")
    public String getAllTransaction(Model model){
        List<TransactionModel> transactionModels = transactionService.getAllTransaction();
        model.addAttribute("transactions", transactionModels);
        List<CategoryTransactionModel> categoryTransactionModels = categoryTransactionService.getAllCategoryTransaction();
        model.addAttribute("categories", categoryTransactionModels);
        List<AccountModel> accountModels = accountService.getAllAccount();
        model.addAttribute("accounts", accountModels);
        return "transaction";
    }

    @PostMapping(value = "/transaction/add", produces = "application/json")
    public @ResponseBody
    TransactionModel addTransaction(@RequestBody Map<String, String> json, Model model){
        TransactionModel transactionModel = new TransactionModel();
        List<CategoryTransactionModel> categoryTransactionModelList = new ArrayList<>();
        CategoryTransactionModel categoryTransactionModel = categoryTransactionService.
                findCategoryTransactionModelByTypeCategory(TypeCategory.valueOf(json.get("typeCategory")));
        categoryTransactionModelList.add(categoryTransactionModel);
        AccountModel accountModel = accountService.getAccountByName(json.get("accountName"));
        String transactionName = json.get("name");
        LocalDateTime transactionDate = LocalDateTime.now();
        transactionModel.setName(transactionName);
        transactionModel.setOperationDate(transactionDate);
        transactionModel.setCategoryTransaction(categoryTransactionModelList);
        transactionModel.setAccountModel(accountModel);
        transactionService.addTransaction(transactionModel);
        model.addAttribute("categoryTransaction",categoryTransactionModelList);
        return transactionModel;
    }
}
