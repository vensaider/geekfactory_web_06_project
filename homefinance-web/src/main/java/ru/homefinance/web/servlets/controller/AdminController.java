package ru.homefinance.web.servlets.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.geekfactory.homefinance.dao.model.UserModel;
import ru.geekfactory.homefinance.service.UserService;

import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/admin")
    public String getAdminManage(Model model){
        List<UserModel> userModelList = userService.getAllUser();
        model.addAttribute("users", userModelList);
        return "admin";
    }
}
