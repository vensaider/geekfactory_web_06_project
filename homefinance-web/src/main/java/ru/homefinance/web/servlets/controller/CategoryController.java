package ru.homefinance.web.servlets.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TypeCategory;
import ru.geekfactory.homefinance.service.CategoryTransactionService;

import java.util.List;
import java.util.Map;

@Controller
public class CategoryController {

    @Autowired
    CategoryTransactionService categoryTransactionService;

    @GetMapping(value = "/category")
    public String getAllCategory(Model model) {
        List<CategoryTransactionModel> categoryTransactionModels = categoryTransactionService.getAllCategoryTransaction();
        model.addAttribute("categories", categoryTransactionModels);
        return "category";
    }

    @PostMapping(value = "/category/add", produces = "application/json")
    public @ResponseBody
    CategoryTransactionModel addCategory(@RequestBody Map<String, String> json){
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setTypeCategory(TypeCategory.valueOf(json.get("typeCategory")));
        categoryTransactionService.addCategoryTransaction(categoryTransactionModel);
        return categoryTransactionModel;
    }

    @PostMapping(value = "/category/update", produces = "application/json")
    public @ResponseBody CategoryTransactionModel updateCategory(@RequestBody Map<String, String> json){
        CategoryTransactionModel categoryTransactionModel = categoryTransactionService.
                getCategoryTransactionById(Long.valueOf(json.get("categoryId"))).get();
        categoryTransactionModel.setTypeCategory(TypeCategory.valueOf(json.get("typeCategory")));
        categoryTransactionService.updateCategory(categoryTransactionModel);
        return categoryTransactionModel;
    }

    @PostMapping(value = "/category/delete", produces = "application/json")
    public @ResponseBody void daleteCategory(@RequestBody Map<String, String> json){
        CategoryTransactionModel categoryTransactionModel = categoryTransactionService.
                getCategoryTransactionById(Long.valueOf(json.get("categoryId"))).get();
        categoryTransactionService.deleteCategoryTransaction(Long.valueOf(json.get("categoryId")));
    }
}

