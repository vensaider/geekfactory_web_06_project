package ru.homefinance.web.servlets.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.AccountType;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.model.CurrencyType;
import ru.geekfactory.homefinance.service.AccountService;
import ru.geekfactory.homefinance.service.CurrencyService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Controller
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private CurrencyService currencyService;

    @GetMapping(value = "/account")
    public String getAllCurrency(Model model) {
        List<AccountModel> accountModels = accountService.getAllAccount();
        model.addAttribute("accounts", accountModels);
        return "account";
    }

    @PostMapping(value = "/account/add", produces = "application/json")
    public @ResponseBody
    AccountModel addAccount(@RequestBody Map<String, String> json){
        CurrencyModel currencyModel = new CurrencyModel();
        currencyModel.setCode(CurrencyType.valueOf(json.get("currency")));
        currencyModel.setSymbol(currencyService.getCurrencySymbol(currencyModel.getCode().toString()));
        CurrencyModel currencyModelCurrent = currencyService.addCurrency(currencyModel);
        AccountModel accountModel = new AccountModel();
        accountModel.setCurrencyModel(currencyModelCurrent);
        accountModel.setAmount(new BigDecimal(json.get("amount")));
        accountModel.setName(json.get("name"));
        accountModel.setType(AccountType.valueOf(json.get("type")));
        accountService.addAccount(accountModel);
        return accountModel;
    }

    @PostMapping(value = "/account/update", produces = "application/json")
    public @ResponseBody AccountModel updateAccount(@RequestBody Map<String, String> json){
        CurrencyModel currencyModel = new CurrencyModel();
        currencyModel.setCode(CurrencyType.valueOf(json.get("currency")));
        currencyModel.setSymbol(currencyService.getCurrencySymbol(currencyModel.getCode().toString()));
        CurrencyModel currencyModelCurrent = currencyService.addCurrency(currencyModel);
        AccountModel accountModel = accountService.getAccountByName(json.get("name"));
        accountModel.setCurrencyModel(currencyModelCurrent);
        accountModel.setAmount(new BigDecimal(json.get("amount")));
        accountModel.setType(AccountType.valueOf(json.get("type")));
        accountService.updateAccount(accountModel);
        return accountModel;
    }

    @PostMapping(value = "/account/delete", produces = "application/json")
    public @ResponseBody void deleteAccount(@RequestBody Map<String, String> json){
        AccountModel accountModel = accountService.getAccountByName(json.get("name"));
        accountService.deleteAccount(accountModel.getAccountId());
    }
}
