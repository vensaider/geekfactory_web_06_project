package ru.homefinance.web.servlets.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.service.CurrencyService;

import java.util.List;

@Controller
public class CurrencyController {

    @Autowired
    private CurrencyService currencyService;

    @GetMapping(value = "/currency")
    public String getAllCurrency(Model model) {
        List<CurrencyModel> currencyModels = currencyService.getAllCurrency();
        model.addAttribute("currencies", currencyModels);
        return "currency";
    }

    @PostMapping(value = "/currency/add", produces = "application/json")
    public @ResponseBody CurrencyModel addCurrency(@RequestBody CurrencyModel currency) {
        String symbol = currencyService.getCurrencySymbol(currency.getCode().toString());
        currency.setSymbol(symbol);
        currencyService.addCurrency(currency);
        return currency;
    }

    @PostMapping(value = "/currency/delete/", produces = "application/json")
    public @ResponseBody CurrencyModel  deleteCurrency(@RequestBody CurrencyModel currencyModel) {
        long id = currencyModel.getCurrencyId();
        currencyService.deleteCurrency(id);
        return currencyModel;
    }

    @PostMapping(value = "/currency/update", produces = "application/json")
    public @ResponseBody CurrencyModel updateCurrency(@RequestBody CurrencyModel currencyModel) {
        String symbol = currencyService.getCurrencySymbol(currencyModel.getCode().toString());
        currencyModel.setSymbol(symbol);
        currencyService.updateCurrency(currencyModel);
        return currencyModel;
    }

}
