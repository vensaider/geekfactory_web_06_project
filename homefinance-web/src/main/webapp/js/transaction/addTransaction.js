function addTransaction() {

    const xhrAddTransaction = new XMLHttpRequest();
    xhrAddTransaction.open("POST", "/transaction/add", true);
    xhrAddTransaction.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrAddTransaction.onreadystatechange = function () {
        if (xhrAddTransaction.readyState != 4 && xhrAddTransaction.status == 200) {
        }
    };
    xhrAddTransaction.send(JSON.stringify({
        name:document.getElementById("transaction-name").value,
        typeCategory: document.getElementById("type-transaction-category").value,
        accountName: document.getElementById("type-transaction-account").value
    }));
}