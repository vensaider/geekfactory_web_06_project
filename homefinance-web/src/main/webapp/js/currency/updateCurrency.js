function updateCurrency() {

    const xhrCurrencyUpdate = new XMLHttpRequest();
    xhrCurrencyUpdate.open("POST", "/currency/update", true);
    xhrCurrencyUpdate.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrCurrencyUpdate.onreadystatechange = function () {
        if (xhrCurrencyUpdate.readyState != 4 && xhrCurrencyUpdate.status == 200) {

            const currencyJson = JSON.parse(xhrCurrencyUpdate.responseText);
            const currencyCode = currencyJson.code;
            const symbols = currencyJson.symbol;
            const currencyIdJson = currencyJson.currencyId;

            currencyJson.symbol = "<img src=\"" + symbols + "\">";
            currencyJson.Edit = "<a id=\"updateCurrency\" href=\"#updateForm\" onclick=\"update()\">Edit</a>";
            currencyJson.Delete = "<form action=\"/currency/delete/\"  method=\"post\" accept-charset=utf-8><button onclick=\"deleteCurrency();return false;\">Delete</button></form>";

            let currencys = [];
            currencys.push(currencyJson);
            let col = [];
            for (let i = 0; i < currencys.length; i++) {
                for (let key in currencys[i]) {
                    if (col.indexOf(key) === -1) {
                        col.push(key);
                    }
                }
            }
            let table = document.getElementById("currency-crud-table");

            let findRow;
            for (let i = 0; i < table.rows.length; i++) {
                for (let j = 0; j < table.rows[i].cells.length; j++)
                    if (table.rows[i].cells[j].firstChild.nodeValue == currencyIdJson) {
                        console.log(table.rows[i].cells[j])
                        findRow = table.rows[i];
                        break;
                    }
            }
            findRow.cells[1].textContent = currencyCode;
            findRow.cells[2].firstChild.setAttribute("src",symbols);
        }
    };
    xhrCurrencyUpdate.send(JSON.stringify({
        currencyId: document.getElementById("message-edit").value,
        code: document.getElementById("message-edit2").value
    }));
}