function deleteCurrency(currentId) {

    const xhrCurrencyDelete = new XMLHttpRequest();
    xhrCurrencyDelete.open("POST", "/currency/delete/", true);
    xhrCurrencyDelete.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrCurrencyDelete.onreadystatechange = function () {
        if (xhrCurrencyDelete.readyState != 4 && xhrCurrencyDelete.status == 200) {
            const currencyJson = JSON.parse(xhrCurrencyDelete.responseText);
            const currencyIdJson = currencyJson.currencyId;


            let table = document.getElementById("currency-crud-table");

            for (let i = 0; i < table.rows.length; i++) {
                for (let j = 0; j < table.rows[i].cells.length; j++) {
                    if (table.rows[i].cells[j].firstChild.nodeValue == currencyIdJson) {
                        table.deleteRow(i);
                        break;
                    }
                }
            }
        }
    };
    xhrCurrencyDelete.send(JSON.stringify({
        currencyId: currentId
    }));
}