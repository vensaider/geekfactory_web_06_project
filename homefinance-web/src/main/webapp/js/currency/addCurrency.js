function currencyAdd() {

    const xhrCurrencyAdd = new XMLHttpRequest();
    xhrCurrencyAdd.open("POST", "/currency/add", true);
    xhrCurrencyAdd.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrCurrencyAdd.onreadystatechange = function () {
        if (xhrCurrencyAdd.readyState != 4 && xhrCurrencyAdd.status == 200) {
            const currencyJson = JSON.parse(xhrCurrencyAdd.responseText);
            const symbols = currencyJson.symbol;
            const currencyIdJson = currencyJson.currencyId;
            currencyJson.symbol = "<img src=\"" + symbols + "\">";
            currencyJson.Edit = "<a id=\"updateCurrency\" href=\"#updateForm\" onclick=\"update()\">Edit</a>";
            currencyJson.Delete = "<form onclick=\"deleteCurrency();return false;\" action=\"/currency/delete/\"  method=\"post\"  accept-charset=utf-8><button id=\"#button" + currencyIdJson + "\" value=\"" + currencyIdJson + "\" onclick=\"getId(this.id);\">Delete</button></form>";
            let currencys = [];
            currencys.push(currencyJson);
            let col = [];

            for (let i = 0; i < currencys.length; i++) {
                for (let key in currencys[i]) {
                    if (col.indexOf(key) === -1) {
                        col.push(key);
                    }
                }
            }
            let table = document.getElementById("currency-crud-table");
            let tr = table.insertRow(-1);                   // TABLE ROW.

            for (let i = 0; i < col.length; i++) {
                let th = document.createElement("th");      // TABLE HEADER.
                th.innerHTML = col[i];
            }
            // ADD JSON DATA TO THE TABLE AS ROWS.
            for (let i = 0; i < currencys.length; i++) {

                tr = table.insertRow(-1);

                for (let j = 0; j < col.length; j++) {
                    let tabCell = tr.insertCell(-1);
                    tabCell.innerHTML = currencys[i][col[j]];
                    if (i >= 2) {

                    }
                }
            }
        }
    };
    xhrCurrencyAdd.send(JSON.stringify({
        code: document.getElementById("message").value
    }));
}