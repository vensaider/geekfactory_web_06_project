function addAccount() {

    const xhrAddAccount = new XMLHttpRequest();
    xhrAddAccount.open("POST", "/account/add", true);
    xhrAddAccount.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrAddAccount.onreadystatechange = function () {
        if (xhrAddAccount.readyState != 4 && xhrAddAccount.status == 200) {
        }
    };
    xhrAddAccount.send(JSON.stringify({
        name: document.getElementById("name").value,
        type: document.getElementById("type").value,
        amount: document.getElementById("amount").value,
        currency: document.getElementById("currency").value
    }));
}