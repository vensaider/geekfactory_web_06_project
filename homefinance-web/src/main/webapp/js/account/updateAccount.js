function updateAccount() {
    const xhrUpdateAccount = new XMLHttpRequest();
    xhrUpdateAccount.open("POST", "/account/update", true);
    xhrUpdateAccount.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrUpdateAccount.onreadystatechange = function () {
        if (xhrUpdateAccount.readyState != 4 && xhrUpdateAccount.status == 200) {
        }
    };
    xhrUpdateAccount.send(JSON.stringify({
        name: document.getElementById("update-name").value,
        type: document.getElementById("update-type").value,
        amount: document.getElementById("update-amount").value,
        currency: document.getElementById("update-currency").value
    }));
}