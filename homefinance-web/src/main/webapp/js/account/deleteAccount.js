function deleteAccount() {
    const xhrDeleteAccount = new XMLHttpRequest();
    xhrDeleteAccount.open("POST", "/account/delete", true);
    xhrDeleteAccount.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrDeleteAccount.onreadystatechange = function () {
        if (xhrDeleteAccount.readyState != 4 && xhrAddAccount.status == 200) {
        }
    };
    xhrDeleteAccount.send(JSON.stringify({
        name: document.getElementById("delete-name").value
    }));
}