function addCategory() {

    const xhrAddCategory = new XMLHttpRequest();
    xhrAddCategory.open("POST", "/category/add", true);
    xhrAddCategory.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrAddCategory.onreadystatechange = function () {
        if (xhrAddCategory.readyState != 4 && xhrAddCategory.status == 200) {
        }
    };
    xhrAddCategory.send(JSON.stringify({
        typeCategory: document.getElementById("type").value
    }));
}