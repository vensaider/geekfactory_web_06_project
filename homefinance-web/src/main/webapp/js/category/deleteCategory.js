function deleteCategory() {

    const xhrDeleteCategory = new XMLHttpRequest();
    xhrDeleteCategory.open("POST", "/category/delete", true);
    xhrDeleteCategory.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrDeleteCategory.onreadystatechange = function () {
        if (xhrDeleteCategory.readyState != 4 && xhrDeleteCategory.status == 200) {
        }
    };
    xhrDeleteCategory.send(JSON.stringify({
        categoryId: document.getElementById("delete-type").value
    }));
}