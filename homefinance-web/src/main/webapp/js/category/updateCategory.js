function updateCategory() {

    const xhrUpdateCategory = new XMLHttpRequest();
    xhrUpdateCategory.open("POST", "/category/update", true);
    xhrUpdateCategory.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrUpdateCategory.onreadystatechange = function () {
        if (xhrUpdateCategory.readyState != 4 && xhrUpdateCategory.status == 200) {
        }
    };
    xhrUpdateCategory.send(JSON.stringify({
        categoryId: document.getElementById("update-categoryById").value,
        typeCategory: document.getElementById("update-type").value
    }));
}