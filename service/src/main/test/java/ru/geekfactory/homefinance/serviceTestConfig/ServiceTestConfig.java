package ru.geekfactory.homefinance.serviceTestConfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.service.CurrencyService;
import ru.geekfactory.homefinance.service.CurrencyServiceImpl;

import static org.mockito.Mockito.mock;

@Configuration
public class ServiceTestConfig {

    @Bean
    public CurrencyRepository currencyRepository(){
        return mock(CurrencyRepository.class);
    }

}
