package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.model.CurrencyType;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.serviceTestConfig.ServiceTestConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceTestConfig.class)
class CurrencyServiceImplTest {

    private CurrencyModel currencyModel = new CurrencyModel();


    @Autowired
    private CurrencyRepository currencyRepository;


    @InjectMocks
    private CurrencyServiceImpl currencyService;

    @Test
    void getAllCurrency() {
        currencyModel.setCurrencyId(1L);
        currencyModel.setCode(CurrencyType.RUB);
        currencyModel.setSymbol("RUB");

        List<CurrencyModel> currencyModelList = new ArrayList<>();
        currencyModelList.add(currencyModel);

        when(currencyRepository.saveAndFlush(currencyModel)).thenReturn(currencyModel);
        when(currencyRepository.findAll()).thenReturn(currencyModelList);

        currencyRepository.saveAndFlush(currencyModel);
        List<CurrencyModel> currencyModelsCheck = currencyRepository.findAll();

        assertEquals(currencyModel, currencyModelsCheck.get(0));
        verify(currencyRepository).findAll();
    }

    @Test
    void getCurrencyById() {
        currencyModel.setCurrencyId(1L);
        currencyModel.setCode(CurrencyType.RUB);
        currencyModel.setSymbol("RUB");

        Optional<CurrencyModel> currencyModelOptional = Optional.of(currencyModel);

        when(currencyRepository.saveAndFlush(currencyModel)).thenReturn(currencyModel);
        when(currencyRepository.findById(1L)).thenReturn(currencyModelOptional);

        currencyRepository.saveAndFlush(currencyModel);
        Optional<CurrencyModel> currencyModelOptionalCheck = currencyRepository.findById(1L);

        assertEquals(currencyModelOptionalCheck.get(), currencyModel);
    }

    @Test
    void addCurrency() {
        currencyModel.setCurrencyId(1L);
        currencyModel.setCode(CurrencyType.RUB);
        currencyModel.setSymbol("RUB");
        Optional<CurrencyModel> currencyModelOptional = Optional.of(currencyModel);

        when(currencyRepository.saveAndFlush(currencyModel)).thenReturn(currencyModelOptional.get());
        when(currencyRepository.findById(1L)).thenReturn(currencyModelOptional);

        currencyRepository.saveAndFlush(currencyModel);

        Optional<CurrencyModel> currencyModelOptional1 =  currencyRepository.findById(1L);
        assertEquals(currencyModelOptional,currencyModelOptional1);
        verify(currencyRepository).saveAndFlush(currencyModel);

    }

    @Test
    void updateCurrency() {
        currencyModel.setCurrencyId(1L);
        currencyModel.setCode(CurrencyType.RUB);
        currencyModel.setSymbol("RUB");

        CurrencyModel currencyModelUpdate = new CurrencyModel();
        currencyModelUpdate.setCurrencyId(1L);
        currencyModelUpdate.setCode(CurrencyType.EUR);
        currencyModelUpdate.setSymbol("EUR");

        Optional<CurrencyModel> currencyModelOptional = Optional.of(currencyModel);

        when(currencyRepository.saveAndFlush(currencyModel)).thenReturn(currencyModel);
        when(currencyRepository.save(currencyModel)).thenReturn(currencyModel);
        when(currencyRepository.findById(1L)).thenReturn(currencyModelOptional);

        currencyRepository.saveAndFlush(currencyModel);
        currencyModel.setCode(currencyModelUpdate.getCode());
        currencyModel.setSymbol(currencyModelUpdate.getSymbol());
        currencyRepository.save(currencyModel);

        assertEquals(currencyModelUpdate,currencyRepository.findById(1L).get());
    }

    @Test
    void deleteCurrency() {
        currencyModel.setCurrencyId(1L);
        currencyModel.setCode(CurrencyType.RUB);
        currencyModel.setSymbol("RUB");

        doNothing().when(currencyRepository).deleteById(1L);

        currencyRepository.deleteById(1L);

        verify(currencyRepository, times(1)).deleteById(1L);
    }

}