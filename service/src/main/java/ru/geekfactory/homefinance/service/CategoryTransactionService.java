package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TypeCategory;

import java.util.List;
import java.util.Optional;

public interface CategoryTransactionService {

    List<CategoryTransactionModel> getAllCategoryTransaction();
    Optional<CategoryTransactionModel> getCategoryTransactionById(long categoryId);
    CategoryTransactionModel addCategoryTransaction(CategoryTransactionModel category);
    CategoryTransactionModel updateCategory(CategoryTransactionModel category);
    void deleteCategoryTransaction(long categoryId);
    CategoryTransactionModel findCategoryTransactionModelByTypeCategory(TypeCategory typeCategory);
}
