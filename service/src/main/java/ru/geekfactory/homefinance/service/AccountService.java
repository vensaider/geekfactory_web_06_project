package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.AccountModel;

import java.util.List;
import java.util.Optional;

public interface AccountService {

    List<AccountModel> getAllAccount();
    Optional<AccountModel> getAccountById(long accountId);
    AccountModel addAccount(AccountModel account);
    AccountModel updateAccount(AccountModel account);
    void deleteAccount(long accountId);
    AccountModel getAccountByName(String name);
}
