package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TypeCategory;
import ru.geekfactory.homefinance.dao.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;


@Service
public class CategoryTransactionServiceImpl implements CategoryTransactionService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<CategoryTransactionModel> getAllCategoryTransaction() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<CategoryTransactionModel> getCategoryTransactionById(long categoryId) {
        return categoryRepository.findById(categoryId);
    }

    @Override
    public CategoryTransactionModel addCategoryTransaction(CategoryTransactionModel category) {
        return categoryRepository.saveAndFlush(category);
    }

    @Override
    public CategoryTransactionModel updateCategory(CategoryTransactionModel category) {
        return categoryRepository.save(category);
    }

    @Override
    public void deleteCategoryTransaction(long categoryId) {
        categoryRepository.deleteById(categoryId);
    }

    @Override
    public CategoryTransactionModel findCategoryTransactionModelByTypeCategory(TypeCategory typeCategory) {
        return categoryRepository.findCategoryTransactionModelByTypeCategory(typeCategory);
    }
}
