package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.repository.AccountRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<AccountModel> getAllAccount() {
        return accountRepository.findAll();
    }

    @Override
    public Optional<AccountModel> getAccountById(long accountId) {
        return accountRepository.findById(accountId);
    }

    @Override
    public AccountModel addAccount(AccountModel account) {
        return accountRepository.saveAndFlush(account);
    }

    @Override
    public AccountModel updateAccount(AccountModel account) {
        return accountRepository.save(account);
    }

    @Override
    public void deleteAccount(long accountId) {
        accountRepository.deleteById(accountId);
    }

    @Override
    public AccountModel getAccountByName(String name) {
        return accountRepository.findAccountModelByName(name);
    }
}
