package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.dao.repository.TransactionRepository;

import java.util.List;
import java.util.Optional;


@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public List<TransactionModel> getAllTransaction() {
        return transactionRepository.findAll();
    }

    @Override
    public Optional<TransactionModel> getTransactionById(long transactionId) {
        return transactionRepository.findById(transactionId);
    }

    @Override
    public TransactionModel addTransaction(TransactionModel transaction) {
        return transactionRepository.saveAndFlush(transaction);
    }

    @Override
    public TransactionModel updateTransaction(TransactionModel transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public void deleteTransaction(long transactionId) {
        transactionRepository.deleteById(transactionId);
    }
}
