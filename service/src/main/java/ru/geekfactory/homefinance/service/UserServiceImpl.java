package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.geekfactory.homefinance.dao.model.UserModel;
import ru.geekfactory.homefinance.dao.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<UserModel> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public Optional<UserModel> getUserById(long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public UserModel addUser(UserModel userModel) {
        return userRepository.saveAndFlush(userModel);
    }

    @Override
    public UserModel updateUser(UserModel userModel) {
        return userRepository.save(userModel);
    }

    @Override
    public void deleteUser(long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public UserModel findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

}
