package ru.geekfactory.homefinance.service.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.geekfactory.homefinance.service")
public class ServiceConfig {
}
