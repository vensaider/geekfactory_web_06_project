package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import java.util.List;
import java.util.Optional;

public interface CurrencyService {

    List<CurrencyModel> getAllCurrency();
    Optional<CurrencyModel> getCurrencyById(long currencyId);
    CurrencyModel addCurrency(CurrencyModel currency);
    CurrencyModel updateCurrency(CurrencyModel currency);
    void deleteCurrency(long currencyId);

    public String getCurrencySymbol(String currencyCode);
}
