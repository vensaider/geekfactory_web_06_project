package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.TransactionModel;

import java.util.List;
import java.util.Optional;

public interface TransactionService {

    List<TransactionModel> getAllTransaction();
    Optional<TransactionModel> getTransactionById(long transactionId);
    TransactionModel addTransaction(TransactionModel transaction);
    TransactionModel updateTransaction(TransactionModel transaction);
    void deleteTransaction(long transactionId);
}
