package ru.geekfactory.homefinance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;

    @Override
    public List<CurrencyModel> getAllCurrency() {
        return currencyRepository.findAll();
    }

    @Override
    public Optional<CurrencyModel> getCurrencyById(long currencyId) {
        return currencyRepository.findById(currencyId);
    }

    @Override
    public CurrencyModel addCurrency(CurrencyModel currency) {
        return currencyRepository.saveAndFlush(currency);
    }

    @Override
    public CurrencyModel updateCurrency(CurrencyModel currency) {
        return currencyRepository.save(currency);
    }

    @Override
    public void deleteCurrency(long currencyId) {
        currencyRepository.deleteById(currencyId);
    }

    @Override
    public String getCurrencySymbol(String currencyCode){
        String symbol = null;
        switch (currencyCode.toString()){
            case "RUB": symbol = "/symbols_icon/RUB.gif";
                break;
            case "EUR": symbol = "/symbols_icon/EUR.gif";
                break;
            case "CNY": symbol = "/symbols_icon/CNY.gif";
                break;
            case "GBP": symbol = "/symbols_icon/GBP.gif";
                break;
            case "JPY": symbol = "/symbols_icon/JPY.gif";
                break;
            case "USD": symbol = "/symbols_icon/USD.gif";
                break;
        }
        return symbol;
    }
}
