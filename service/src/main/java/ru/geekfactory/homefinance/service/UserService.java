package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.UserModel;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<UserModel> getAllUser();
    Optional<UserModel> getUserById(long userId);
    UserModel addUser(UserModel userModel);
    UserModel updateUser(UserModel userModel);
    void deleteUser(long userId);
    UserModel findByUserName(String userName);
}
